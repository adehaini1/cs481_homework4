﻿using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnimalsList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AnimalDescription : ContentPage
	{
		public  AnimalsListModel vbn { get; set; }
		List<AnimalsListModel> ghj = new List<AnimalsListModel>();
		public AnimalDescription(string MyAnimalName)
		{
			InitializeComponent();
			Animals animals = new Animals();
			ghj = animals.data.Where(x => x.AnimalName == MyAnimalName).ToList();			
			AnimalName.Text = ghj[0].AnimalName.ToString();
			AnimalImage.Source = ghj[0].AnimalsImage.ToString();
			AnimalsDescription.Text = ghj[0].AnimalDescription.ToString();			
		}
		public AnimalDescription(string MyAnimalDescription,string MyAnimalName, string MyAnimalsImage )
		{
			InitializeComponent();
			AnimalName.Text = MyAnimalName;
			AnimalImage.Source = MyAnimalsImage;
			AnimalsDescription.Text = MyAnimalDescription;
		}
	}
}